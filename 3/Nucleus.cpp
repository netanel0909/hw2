#include "Nucleus.h"

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	_start = start;
	_end = end;
	_on_complementary_dna_strand = on_complementary_dna_strand;
}

//getters
bool Gene::is_on_complementary_dna_strand(void) const
{
	return _on_complementary_dna_strand;
}

unsigned int Gene::get_end(void) const
{
	return _end;
}

unsigned int Gene::get_start(void) const
{
	return _start;
}

//setters
void Gene::set_on_complementary_dna_strand(bool on_complementary_dna_strand)
{
	_on_complementary_dna_strand = on_complementary_dna_strand;
}

void Gene::set_end(unsigned int end)
{
	_end = end;
}

void Gene::set_start(unsigned int start)
{
	this->_start = start;
}

void Nucleus::init(const std::string dna_sequence)
{
	int i = 0;
	_DNA_strand = dna_sequence;
	_complementary_DNA_strand = "";
	for (i; i < dna_sequence.length(); i++)
	{
		if (dna_sequence[i] == 'A' || dna_sequence[i] == 'T')
		{
			this->_complementary_DNA_strand += dna_sequence[i] == 'A' ? 'T' : 'A'; // checks if a or t, if a puts t if t puts a
		}
		else if (dna_sequence[i] == 'G' || dna_sequence[i] == 'C')
		{
			this->_complementary_DNA_strand += dna_sequence[i] == 'G' ? 'C' : 'G'; // checks if a or c, if c puts g if g puts c
		}
		else 
		{
			std::cerr<<"not only A,C,G,T";
			_exit(1);
		}
	}
}

std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	std::string return_rna = "";
	std::string curr_strand = "";

	curr_strand = gene.is_on_complementary_dna_strand() ? _complementary_DNA_strand : _DNA_strand;//if gene true puts DNA else puts its opposite

	for (unsigned int i = gene.get_start(); i <= gene.get_end(); i++)
	{
		if (curr_strand[i] != 'T')
		{
			return_rna += curr_strand[i];
		}
		else
		{
			return_rna += 'U';
		}
	}
	return return_rna;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string return_val = "";
	for (int i = 0; i <= _DNA_strand.length(); i++)
	{
		if (_DNA_strand[i] == 'A' || _DNA_strand[i] == 'T')
		{
			return_val += _DNA_strand[i] == 'A' ? 'G' : 'C'; // checks if a or t, if a puts t if t puts a
		}
		if (_DNA_strand[i] == 'G' || _DNA_strand[i] == 'C')
		{
			return_val += _DNA_strand[i] == 'G' ? 'A' : 'T'; // checks if a or c, if c puts g if g puts c
		}
	}

	return return_val;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string & codon) const
{
	unsigned int count = 0;
	int pos = -3;

	while ((pos = _DNA_strand.find(codon, pos + 3)) != -1) //checks if substring in string
	{
		count++;//updates counter
	}
	return count;
}


