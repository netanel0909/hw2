#include "Mitochondrion.h"

void Mitochondrion::init()
{
	_glocuse_level = 0;
	_has_glocuse_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	int i = 0;
	int arr[] = {ALANINE,LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE, AMINO_CHAIN_END};

	AminoAcidNode* temp = new AminoAcidNode;

	if (protein.get_first())
	{
		temp = protein.get_first();
		while (i < (sizeof(arr) / sizeof(int)))
		{
			if (temp->get_data() != arr[i])
			{
				this->_has_glocuse_receptor = false;
				return;
			}

			temp = temp->get_next();

			i++;
		}
	}
	this->_has_glocuse_receptor = true;
	return;

}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

bool Mitochondrion::produceATP(const int glocuse_unit) const
{
	return (_has_glocuse_receptor && _glocuse_level >= 50);
}
