#include "Ribosome.h"

Protein * Ribosome::create_protein(std::string &  RNA_transcript) const
{
	std::string rna = RNA_transcript;
	Protein* return_val = new Protein;
	return_val->init();
	std::string temp = "";
	while (rna.length() >= 3)
	{
		temp = rna.substr(0, 3);
		if (get_amino_acid(temp) == UNKNOWN)
		{
			std::cerr << "No such amino acid" << std::endl;
			delete(return_val);
			return_val = nullptr;
			return nullptr;
		}
		else
		{
			return_val->add(get_amino_acid(temp));
			rna = rna.substr(3, rna.length() - 3);
		}
	}
	return return_val;
}
