#pragma once

#include<iostream>
#include<string>
#include "Protein.h"
#include "Nucleus.h"
#include"AminoAcid.h"
#include "Ribosome.h"
class Mitochondrion
{
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;

	public:

		void init();//initiates mitochondrion
		void insert_glucose_receptor(const Protein & protein);//inserts new glucose receptor
		void set_glucose(const unsigned int glocuse_units);//set function
		bool produceATP(const int glocuse_unit) const;//Atp prouduce function for cell

};

