#pragma once
#include <iostream>
#include <string>

class Gene
{

	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;

	public:

		void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);

		//getters
		bool is_on_complementary_dna_strand(void) const;
		unsigned int get_end(void) const;
		unsigned int get_start(void) const;

		//setters
		void set_on_complementary_dna_strand(bool);
		void set_end(unsigned int);
		void set_start(unsigned int);
	
};

class Nucleus
{
	std::string _DNA_strand;
	std::string _complementary_DNA_strand;

	public:

		//initiating Nuclues
		void init(const std::string dna_sequence);//initiates 
		std::string get_RNA_transcript(const Gene& gene) const;//gets the RNA from gene
		std::string get_reversed_DNA_strand() const;//reverses DNA or its opposite
		unsigned int get_num_of_codon_appearances(const std::string& codon) const;//gets amount of codons in DNA

};
