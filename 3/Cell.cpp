#include "Cell.h"

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	_glocus_receptor_gene = glucose_receptor_gene;
	_nucleus.init(dna_sequence);
	_mitochondrion.init();
}

bool Cell::get_ATP()
{
	std::string temp = _nucleus.get_RNA_transcript(_glocus_receptor_gene);
	Protein* protein = _ribosome.create_protein(temp);
	bool return_val = false;
	if (protein == nullptr)
	{
		std::cerr << "no protein";
		_exit(1);
	}
	else
	{
		_mitochondrion.insert_glucose_receptor(*protein);
		_mitochondrion.set_glucose(100);

		if (_mitochondrion.produceATP(_atp_units))
		{
			_atp_units = 100;
			return_val = true;
		}
	}
	return return_val;
}

